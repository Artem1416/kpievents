package ua.kpi.kpievents.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ua.kpi.kpievents.model.Role;
import ua.kpi.kpievents.model.User;
import ua.kpi.kpievents.service.RoleService;
import ua.kpi.kpievents.service.UserService;

/**
 * Controller for Admin's pages (list of users, project management, etc.)
 *
 * @author Eugene Suleimanov
 */

@Controller
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String admin(Model model) {
        return "admin/adminHome";
    }

    @RequestMapping(value = "/admin/listRoles", method = RequestMethod.GET)
    public String listRoles(Model model) {
        model.addAttribute("role", new Role());
        model.addAttribute("listRoles", this.roleService.getAll());

        return "admin/role/listRoles";
    }

    @RequestMapping(value = "/admin/listUsers", method = RequestMethod.GET)
    public String listUsers(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("listUsers", this.userService.getAll());

        return "admin/user/listUsers";
    }
}
