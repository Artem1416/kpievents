package ua.kpi.kpievents.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.User;
import ua.kpi.kpievents.service.SecurityService;
import ua.kpi.kpievents.service.UserService;
import ua.kpi.kpievents.validator.UserRegistrationValidator;

/**
 * Controller for pages connected with authentication (registration, forgot password, etc.)
 *
 * @author Eugene Suleimanov
 */

@Controller
@SessionAttributes(value = "userForm")
public class AuthenticationController {

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserRegistrationValidator userValidator;

    @ModelAttribute("userForm")
    public User createUser(){
        return new User();
    }

    @RequestMapping(value = "/signUp", method = RequestMethod.GET)
    public String authorization(Model model) {
        model.addAttribute("userForm", new User());

        return "authentication/signUp";
    }

    @RequestMapping(value = "/signUp", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm,
                               BindingResult bindingResult) throws BadRequestException {

        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "authentication/signUp";
        }

        userService.save(userForm);

        securityService.autoLogin(userForm.getUsername(), userForm.getConfirmPassword());

        return "welcome";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null) {
            model.addAttribute("error", "Username or password is incorrect.");
        }

        if (logout != null) {
            model.addAttribute("message", "Logged out successfully.");
        }

        return "authentication/login";
    }
}
