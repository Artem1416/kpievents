package ua.kpi.kpievents.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.User;
import ua.kpi.kpievents.service.UserService;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by Yuriy Phediv on 23.03.2017.
 */
@RestController
@RequestMapping("/rest/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable("id") UUID id) throws BadRequestException {
        return new ResponseEntity(userService.getById(id), HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Void> saveUser(@RequestBody User user){
        userService.save(user);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<Void> updateUser(@RequestBody User user){
        userService.update(user);
        return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
    }

    @GetMapping("/byname/{name}")
    public ResponseEntity<User> findByName(@PathVariable("name") String name) throws BadRequestException{
        return new ResponseEntity(userService.findByUserName(name),HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<Collection<User>> allUsers() {
        return new ResponseEntity(userService.getAll(), HttpStatus.OK);
    }


}
