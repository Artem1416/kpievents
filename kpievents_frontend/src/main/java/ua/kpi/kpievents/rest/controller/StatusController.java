package ua.kpi.kpievents.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Status;
import ua.kpi.kpievents.service.StatusService;

import java.util.UUID;

/**
 * Created by Yuriy Phediv on 23.03.2017.
 */
@RestController
@RequestMapping("/rest/status")
public class StatusController {
    
    @Autowired
    private StatusService statusService;

    @GetMapping("/all")
    public ResponseEntity allStatus() {
        return new ResponseEntity(statusService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable("id") UUID id) throws BadRequestException {
        return new ResponseEntity(statusService.getById(id),HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Void> saveStatus(@RequestBody Status status){
        statusService.save(status);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @DeleteMapping("/remove")
    public ResponseEntity<Void> removeStatus(@RequestBody Status status){
        statusService.remove(status);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @GetMapping("/byname/{name}")
    public ResponseEntity<Status> findByName(@PathVariable("name") String name) throws BadRequestException{
        return new ResponseEntity(statusService.findByName(name),HttpStatus.OK);
    }
}
