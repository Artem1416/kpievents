package ua.kpi.kpievents.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Event;
import ua.kpi.kpievents.model.User;
import ua.kpi.kpievents.service.EventService;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by Yuriy Phediv on 23.03.2017.
 */
@RestController
@RequestMapping("/event")
public class EventController {
    
    @Autowired
    private EventService eventService;


    @GetMapping("/{id}")
    public ResponseEntity<Event> getById(@PathVariable("id") UUID id) throws BadRequestException{
        return new ResponseEntity(eventService.getById(id),HttpStatus.OK);
    }

    @GetMapping(value = "/all")
    public ResponseEntity<Collection<Event>> allEvent() {
        return new ResponseEntity(eventService.getAll(),HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Void> saveEvent(@RequestBody Event event){
        eventService.save(event);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @DeleteMapping("/remove")
    public ResponseEntity<Void> removeEvent(@RequestBody Event Event){
        eventService.remove(Event);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @GetMapping("/byname/{name}")
    public ResponseEntity<Event> findEventByName(@PathVariable("name") String name) throws BadRequestException{
        return new ResponseEntity(eventService.findEventByName(name),HttpStatus.OK);
    }

    @GetMapping("/byuser")
    public ResponseEntity<Collection<Event>> findEventsByUser(@RequestBody User user){
        return new ResponseEntity(eventService.findEventsByCreatedUser(user),HttpStatus.OK);
    }

  
}
