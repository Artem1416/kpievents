package ua.kpi.kpievents.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Role;
import ua.kpi.kpievents.service.RoleService;

import java.util.UUID;

/**
 * Created by Yuriy Phediv on 23.03.2017.
 */
@RestController
@RequestMapping("/rest/role")
public class RoleController {
    
    @Autowired
    private RoleService roleService;

    @GetMapping("/all")
    public ResponseEntity allRole() {
        return new ResponseEntity(roleService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable("id") UUID id) throws BadRequestException {
        return new ResponseEntity(roleService.getById(id),HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Void> saveRole(@RequestBody Role role){
        roleService.save(role);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @DeleteMapping("/remove")
    public ResponseEntity<Void> removeRole(@RequestBody Role role){
        roleService.remove(role);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
