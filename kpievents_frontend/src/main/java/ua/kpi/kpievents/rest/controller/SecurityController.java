package ua.kpi.kpievents.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.rest.request.UserLoginRequest;
import ua.kpi.kpievents.service.SecurityService;

/**
 * Created by Yuriy Phediv on 23.03.2017.
 */
@RestController
@RequestMapping("/rest/security")
public class SecurityController {

    @Autowired
    private SecurityService securityService;

    @PostMapping("/autologin")
    public ResponseEntity<Void> autologin(@RequestBody UserLoginRequest req) throws BadRequestException{
        this.securityService.autoLogin(req.getUsername(), req.getPassword());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
