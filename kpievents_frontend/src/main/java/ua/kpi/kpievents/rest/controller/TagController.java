package ua.kpi.kpievents.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Tag;
import ua.kpi.kpievents.service.TagService;

import java.util.UUID;

/**
 * Created by Yuriy Phediv on 23.03.2017.
 */
@RestController
@RequestMapping("/rest/tag")
public class TagController {
    
    @Autowired
    private TagService tagService;

    @GetMapping("/all")
    public ResponseEntity allTag() {
        return new ResponseEntity(tagService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable("id") UUID id) throws BadRequestException {
        return new ResponseEntity(tagService.getById(id),HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Void> saveTag(@RequestBody Tag tag){
        tagService.save(tag);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @DeleteMapping("/remove")
    public ResponseEntity<Void> removeTag(@RequestBody Tag tag){
        tagService.remove(tag);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @GetMapping("/byname/{name}")
    public ResponseEntity<Tag> findByName(@PathVariable("name") String name) throws BadRequestException{
        return new ResponseEntity(tagService.findByName(name),HttpStatus.OK);
    }

}
