package ua.kpi.kpievents.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Comment;
import ua.kpi.kpievents.model.User;
import ua.kpi.kpievents.service.CommentService;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by Yuriy Phediv on 23.03.2017.
 */
@RestController
@RequestMapping("/rest/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @GetMapping("/all")
    public ResponseEntity<Collection<Comment>> allComment() {
        return new ResponseEntity(commentService.getAll(),HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Comment> getById(@PathVariable("id") UUID id) throws BadRequestException {
        return new ResponseEntity(commentService.getById(id),HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Void> saveComment(@RequestBody Comment Comment) {
        commentService.save(Comment);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @DeleteMapping("/remove")
    public ResponseEntity<Void> removeComment(@RequestBody Comment Comment){
        commentService.remove(Comment);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @GetMapping("/byauthor}")
    public ResponseEntity<Collection<Comment>> findByAuthor(@RequestBody User author){
        return new ResponseEntity(commentService.findByAuthor(author),HttpStatus.OK);
    }
}
