package ua.kpi.kpievents.rest.controller;

import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.kpi.kpievents.rest.response.TokenResponse;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Yuriy Phediv on 23.03.2017.
 */
@RestController
@RequestMapping("/rest")
public class CsrfController {
    @GetMapping("/csrf-token")
    public TokenResponse getCsrfToken(HttpServletRequest request) {
        CsrfToken token = (CsrfToken) request.getAttribute(CsrfToken.class.getName());
        return new TokenResponse(token.getToken());
    }
}
