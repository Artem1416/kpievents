package ua.kpi.kpievents.rest.response;

/**
 * Created by Yuriy Phediv on 23.03.2017.
 */
public class TokenResponse {
    private String token;

    public TokenResponse(String token){
        this.token = token;
    }
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
