package ua.kpi.kpievents.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.rest.response.ErrorResponse;

/**
 * Created by Yuriy Phediv on 23.03.2017.
 */

@ControllerAdvice
public class GlobalExceptionHandler{

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(BadRequestException ex) {
        ErrorResponse error = new ErrorResponse(HttpStatus.BAD_REQUEST.value(), ex.getErrorMsg());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
    }


}
