package ua.kpi.kpievents.rest.request;

/**
 * Created by Yuriy Phediv on 23.03.2017.
 */
public class UserLoginRequest {
    private String username;
    private String password;

    public UserLoginRequest(String username, String password){
        this.username = username;
        this.password = password;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
