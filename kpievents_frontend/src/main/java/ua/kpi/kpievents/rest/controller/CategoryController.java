package ua.kpi.kpievents.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Category;
import ua.kpi.kpievents.service.CategoryService;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by Yuriy Phediv on 22.03.2017.
 */
@RestController
@RequestMapping("/rest/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/all")
    public ResponseEntity<Collection<Category>> allCategory() {
        return new ResponseEntity(categoryService.getAll(),HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Category> getById(@PathVariable("id") UUID id) throws BadRequestException{
        return new ResponseEntity(categoryService.getById(id),HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Void> saveCategory(@RequestBody Category category){
        categoryService.save(category);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @DeleteMapping("/remove")
    public ResponseEntity<Void> removeCategory(@RequestBody Category category){
        categoryService.remove(category);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @GetMapping("/byname/{name}")
    public ResponseEntity<Category> findByName(@PathVariable("name") String name) throws BadRequestException{
        return new ResponseEntity(categoryService.findByName(name),HttpStatus.OK);
    }

}
