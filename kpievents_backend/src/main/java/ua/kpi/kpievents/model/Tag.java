package ua.kpi.kpievents.model;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Set;

/**
 * @author Alexey Yefimenko
 */

@Entity
@Table(name = "tags")
public class Tag extends NamedEntity{

    @ManyToMany(mappedBy = "tags")
    private Set<Event> events;

}
