package ua.kpi.kpievents.model;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Alexey Yefimenko
 */
@Entity
@Table(name = "statuses")
public class Status extends NamedEntity{

    @OneToMany(mappedBy = "status")
    private Set<Event> events;

}
