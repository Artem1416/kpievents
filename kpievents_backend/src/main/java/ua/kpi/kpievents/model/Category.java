package ua.kpi.kpievents.model;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Alexey Yefimenko
 */

@Entity
@Table(name = "categories")
public class Category extends NamedEntity{

    @OneToMany(mappedBy = "category")
    private Set<Event> events;

}
