package ua.kpi.kpievents.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kpi.kpievents.dao.EventDao;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Category;
import ua.kpi.kpievents.model.Event;
import ua.kpi.kpievents.model.User;
import ua.kpi.kpievents.service.EventService;

import javax.persistence.EntityExistsException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */
@Service
@Transactional
public class EventServiceImpl implements EventService{

    @Autowired
    private EventDao eventDao;

    @Override
    public void save(Event event){
        eventDao.save(event);
    }

    @Override
    public void remove(Event event) {
        eventDao.remove(event);
    }

    @Override
    public Event findEventByName(String name) throws BadRequestException {
        try{
            return eventDao.findEventByName(name);
        }catch(NoResultException nre){
            throw new BadRequestException(String.format("Event with name = %s doesn't exist",name));
        }
    }

    @Override
    public Collection<Event> findEventsByCreatedUser(User user) {
        return eventDao.findEventsByCreatedUser(user);
    }

    @Override
    public Collection<Event> findEventsByDate(Date startDate, Date endDate) {
        return eventDao.findEventsByDate(startDate, endDate);
    }

    @Override
    public Event getById(UUID id) throws BadRequestException {
        try{
            return eventDao.getById(id);
        }catch(NoResultException nre){
            throw new BadRequestException(String.format("Event with id = %s doesn't exist",id));
        }
    }

    @Override
    public Collection<Event> getAll() {
        return eventDao.getAll();
    }
}
