package ua.kpi.kpievents.service;

import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Category;
import ua.kpi.kpievents.model.Event;
import ua.kpi.kpievents.model.User;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */
public interface EventService {
    Event getById(UUID id) throws BadRequestException;

    Collection<Event> getAll();

    void save(Event event);

    void remove(Event event);

    Event findEventByName(String name) throws BadRequestException;

    Collection<Event> findEventsByCreatedUser(User user);

    Collection<Event> findEventsByDate(Date startDate, Date endDate);
}
