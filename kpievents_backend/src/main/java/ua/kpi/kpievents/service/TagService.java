package ua.kpi.kpievents.service;

import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Tag;

import java.util.Collection;
import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */
public interface TagService {

    Tag getById(UUID id) throws BadRequestException;

    Collection<Tag> getAll();

    void save(Tag tag);

    void remove(Tag tag);

    Tag findByName(String name) throws BadRequestException;

}
