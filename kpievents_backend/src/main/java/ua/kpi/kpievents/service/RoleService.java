package ua.kpi.kpievents.service;


import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Role;

import java.util.Collection;
import java.util.UUID;

/**
 * Service interface for class {@link Role}.
 *
 * @author Eugene Suleimanov
 */
public interface RoleService {
    Role getById(UUID id) throws BadRequestException;

    Role findByName(String name);

    Collection<Role> getAll();

    void save(Role role);

    void remove(Role role);
}
