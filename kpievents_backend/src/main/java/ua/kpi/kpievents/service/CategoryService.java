package ua.kpi.kpievents.service;

import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Category;

import java.util.Collection;
import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */
public interface CategoryService {

    Category getById(UUID id) throws BadRequestException;

    Collection<Category> getAll();

    void save(Category category);

    void remove(Category category);

    Category findByName(String name) throws BadRequestException;

}
