package ua.kpi.kpievents.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kpi.kpievents.dao.StatusDao;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Status;
import ua.kpi.kpievents.service.StatusService;

import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */
@Service
@Transactional
public class StatusServiceImpl implements StatusService{

    @Autowired
    private StatusDao statusDao;

    @Override
    public Status findByName(String statusName) throws BadRequestException {
        try {
            return statusDao.findByStatusName(statusName);
        }catch(NoResultException nre){
            throw new BadRequestException(String.format("Status with name = %s doesn't exist",statusName));
        }
    }

    @Override
    public Status getById(UUID id) throws BadRequestException {
        try {
            return statusDao.getById(id);
        }catch(NoResultException nre){
            throw new BadRequestException(String.format("Status with id = %s doesn't exist",id));
        }
    }

    @Override
    public Collection<Status> getAll() {
        return statusDao.getAll();
    }

    @Override
    public void save(Status status) {
        statusDao.save(status);
    }

    @Override
    public void remove(Status status) {
        statusDao.remove(status);
    }
}
