package ua.kpi.kpievents.service;

import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Status;

import java.util.Collection;
import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */
public interface StatusService {

    Status getById(UUID id) throws BadRequestException;

    Collection<Status> getAll();

    void save(Status status);

    void remove(Status status);

    Status findByName(String statusName) throws BadRequestException;

}
