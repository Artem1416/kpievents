package ua.kpi.kpievents.service;

import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.User;

import java.util.Collection;
import java.util.UUID;

/**
 * service class for {@link User}
 *
 * @author Eugene Suleimanov
 */

public interface UserService {

    void save(User user);

    void update(User user);

    User getById(UUID id) throws BadRequestException;

    User findByUserName(String username) throws BadRequestException;

    Collection<User> getAll();

    boolean coincidencePassword(CharSequence rawPassword, String encodedPassword);
}
