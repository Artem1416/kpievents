

package ua.kpi.kpievents.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kpi.kpievents.dao.RoleDAO;
import ua.kpi.kpievents.dao.UserDAO;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Role;
import ua.kpi.kpievents.model.User;
import ua.kpi.kpievents.service.UserService;

import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Implementation of {@link UserService }interface
 *
 * @author Eugene Suleimanov
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDao;

    @Autowired
    private RoleDAO roleDao;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    @Transactional
    public void save(User user){
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Set<Role> roles = new HashSet<>();
        roles.add(roleDao.findByName("ROLE_USER"));
        user.setRoles(roles);
        userDao.save(user);
    }

    @Override
    @Transactional
    public void update(User user) {
        Set<Role> roles = new HashSet<>();
        roles.add(roleDao.findByName("ROLE_USER"));
        user.setRoles(roles);
        userDao.save(user);
    }

    @Override
    @Transactional
    public User getById(UUID id) throws BadRequestException{
        try {
            return userDao.getById(id);
        }catch(NoResultException nre){
            throw new BadRequestException(String.format("User with id = %s doesn't exist",id));
        }
    }

    @Override
    @Transactional
    public User findByUserName(String username) throws BadRequestException{
        try {
            return userDao.findByUserName(username);
        }catch(NoResultException nre){
            throw new BadRequestException(String.format("User with name = %s doesn't exist",username));
        }
    }

    @Override
    public Collection<User> getAll() {
        return userDao.getAll();
    }

    @Override
    public boolean coincidencePassword(CharSequence rawPassword, String encodedPassword) {
        return bCryptPasswordEncoder.matches(rawPassword, encodedPassword);
    }


}