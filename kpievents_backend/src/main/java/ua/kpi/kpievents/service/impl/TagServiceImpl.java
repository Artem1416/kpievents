package ua.kpi.kpievents.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kpi.kpievents.dao.TagDao;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Tag;
import ua.kpi.kpievents.service.TagService;

import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */
@Service
@Transactional
public class TagServiceImpl implements TagService{

    @Autowired
    private TagDao tagDao;

    @Override
    public Tag getById(UUID id) throws BadRequestException {
        try {
            return tagDao.getById(id);
        }catch(NoResultException nre){
            throw new BadRequestException(String.format("Tag with id = %s doesn't exist",id));
        }
    }

    @Override
    public Collection<Tag> getAll() {
        return tagDao.getAll();
    }

    @Override
    public void save(Tag tag) {
        tagDao.save(tag);
    }

    @Override
    public void remove(Tag tag) {
        tagDao.remove(tag);
    }

    @Override
    public Tag findByName(String name) throws BadRequestException {
        try {
            return tagDao.findByName(name);
        }catch(NoResultException nre){
            throw new BadRequestException(String.format("Taf with name = %s doesn't exist",name));
        }
    }
}
