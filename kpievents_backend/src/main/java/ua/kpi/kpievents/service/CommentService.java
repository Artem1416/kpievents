package ua.kpi.kpievents.service;

import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Comment;
import ua.kpi.kpievents.model.User;

import java.util.Collection;
import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */
public interface CommentService {

    Comment getById(UUID id) throws BadRequestException;

    Collection<Comment> getAll();

    void save(Comment comment);

    void remove(Comment comment);

    Collection<Comment> findByAuthor(User author);

}
