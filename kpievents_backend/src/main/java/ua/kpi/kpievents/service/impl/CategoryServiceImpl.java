package ua.kpi.kpievents.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kpi.kpievents.dao.CategoryDao;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Category;
import ua.kpi.kpievents.service.CategoryService;

import javax.persistence.EntityExistsException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import java.util.Collection;
import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */
@Service
@Transactional
public class CategoryServiceImpl implements CategoryService{

    @Autowired
    private CategoryDao categoryDao;

    @Override
    public Category getById(UUID id) throws BadRequestException {
        try {
            return categoryDao.getById(id);
        }catch(NoResultException nre){
            throw new BadRequestException(String.format("Category with id = %s doesn't exist",id));
        }
    }

    @Override
    public Collection<Category> getAll(){
        return categoryDao.getAll();
    }

    @Override
    public void save(Category category) {
        categoryDao.save(category);
    }

    @Override
    public void remove(Category category) {
        categoryDao.remove(category);
    }

    @Override
    public Category findByName(String name)throws BadRequestException {
        try {
            return categoryDao.findCategoryByName(name);
        }catch(NoResultException nre){
            throw new BadRequestException(String.format("Category with name = %s doesn't exist",name));
        }
    }
}
