package ua.kpi.kpievents.service;

/**
 * Interface for messaging functionality.
 *
 * @author Eugene Suleimanov
 */

public interface MessageService {

    String getMessage(String key);
}
