package ua.kpi.kpievents.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kpi.kpievents.dao.CommentDao;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Comment;
import ua.kpi.kpievents.model.User;
import ua.kpi.kpievents.service.CommentService;

import javax.persistence.EntityExistsException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import java.util.Collection;
import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */
@Service
@Transactional
public class CommentServiceImpl implements CommentService{

    @Autowired
    private CommentDao commentDao;

    @Override
    public Comment getById(UUID id) throws BadRequestException {
        try {
            return commentDao.getById(id);
        }catch(NoResultException nre){
            throw new BadRequestException(String.format("Comment with id = %s doesn't exist",id));
        }
    }

    @Override
    public Collection<Comment> getAll() {
        return commentDao.getAll();
    }

    @Override
    public void save(Comment comment){
        commentDao.save(comment);
    }

    @Override
    public void remove(Comment comment) {
        commentDao.remove(comment);
    }

    @Override
    public Collection<Comment> findByAuthor(User author) {
        return commentDao.findByAuthor(author);
    }
}
