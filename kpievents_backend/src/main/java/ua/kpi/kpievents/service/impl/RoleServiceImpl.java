package ua.kpi.kpievents.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kpi.kpievents.dao.RoleDAO;
import ua.kpi.kpievents.exception.BadRequestException;
import ua.kpi.kpievents.model.Role;
import ua.kpi.kpievents.service.RoleService;

import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.UUID;

/**
 * Implementation of {@link RoleService} interface.
 *
 * @author Eugene Suleimanov
 */

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDAO roleDAO;

    @Override
    @Transactional
    public Role getById(UUID id) throws BadRequestException{
        try {
            return roleDAO.getById(id);
        }catch(NoResultException e){
            throw new BadRequestException(String.format("Role with id = %s doesn't exist",id));
        }
    }

    @Override
    @Transactional
    public Role findByName(String name){
        return roleDAO.findByName(name);
    }

    @Override
    @Transactional
    public Collection<Role> getAll() {
        return roleDAO.getAll();
    }

    @Override
    @Transactional
    public void save(Role role) {
        roleDAO.save(role);
    }

    @Override
    @Transactional
    public void remove(Role role) {
        roleDAO.remove(role);
    }
}