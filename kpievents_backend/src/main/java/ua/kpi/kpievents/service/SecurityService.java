package ua.kpi.kpievents.service;

import ua.kpi.kpievents.exception.BadRequestException;

/**
 * Service for security
 *
 * @author Eugene Suleimanov
 */

public interface SecurityService {

    String findLoggedInUsername();

    void autoLogin(String username, String password) throws BadRequestException;
}
