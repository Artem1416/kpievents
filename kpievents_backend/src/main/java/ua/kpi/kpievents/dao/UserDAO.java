package ua.kpi.kpievents.dao;


import ua.kpi.kpievents.model.User;

import java.util.UUID;

/**
 * Extension of {@link GenericDAO} interface for class {@link User}.
 *
 * @author Eugene Suleimanov
 */
public interface UserDAO extends GenericDAO<User, UUID> {

    User findByUserName(String username);
}
