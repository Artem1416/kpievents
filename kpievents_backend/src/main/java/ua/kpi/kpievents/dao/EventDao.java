package ua.kpi.kpievents.dao;

import ua.kpi.kpievents.model.Category;
import ua.kpi.kpievents.model.Event;
import ua.kpi.kpievents.model.User;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Admin on 02.02.2017.
 */
public interface EventDao extends GenericDAO<Event, UUID> {

    Event findEventByName(String name);

    Collection<Event> findEventsByCreatedUser(User user);

    Collection<Event> findEventsByDate(Date startDate, Date endDate);

    Collection<Event> findEventsByCategory(Category category);
}
