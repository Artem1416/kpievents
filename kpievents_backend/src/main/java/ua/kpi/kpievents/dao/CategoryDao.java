package ua.kpi.kpievents.dao;

import ua.kpi.kpievents.model.Category;
import ua.kpi.kpievents.model.Event;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by Admin on 04.02.2017.
 */

public interface CategoryDao extends GenericDAO<Category, UUID>{

    Category findCategoryByName(String name);

}
