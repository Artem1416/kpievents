package ua.kpi.kpievents.dao;

import ua.kpi.kpievents.model.Tag;

import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */
public interface TagDao extends GenericDAO<Tag, UUID>{

    Tag findByName(String name);

}
