package ua.kpi.kpievents.dao;


import ua.kpi.kpievents.model.Role;

import java.util.UUID;

/**
 * Extension of {@link GenericDAO} interface for class {@link Role}.
 *
 * @author Eugene Suleimanov
 */
public interface RoleDAO extends UniqueName<Role, UUID> {
}
