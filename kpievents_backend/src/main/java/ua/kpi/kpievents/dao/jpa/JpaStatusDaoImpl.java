package ua.kpi.kpievents.dao.jpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import ua.kpi.kpievents.dao.StatusDao;
import ua.kpi.kpievents.model.Status;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */

@Repository
public class JpaStatusDaoImpl implements StatusDao{

    @PersistenceContext
    private EntityManager entityManager;

    private static final Logger logger = LoggerFactory.getLogger(JpaStatusDaoImpl.class);

    @Override
    public Status findByStatusName(String name) {
        Query query = entityManager.createQuery("SELECT status FROM Status status WHERE status.name = :name");
        query.setParameter("name", name);

        Status status = (Status) query.getSingleResult();
        logger.info("Status successfully loaded. Result: " + status);
        return status;
    }

    @Override
    public Status getById(UUID id) {
        Query query = entityManager.createQuery("SELECT status FROM Status status WHERE status.id = :id");
        query.setParameter("id", id);
        Status status = (Status) query.getSingleResult();
        logger.info("Status successfully loaded. Result: " + status);

        return status;
    }

    @Override
    public Collection<Status> getAll() {
        Query query = entityManager.createQuery("SELECT status FROM Status status");
        Collection<Status> statuses = query.getResultList();
        for (Status s:statuses) {
            logger.info("Result list: " + s);
        }
        return statuses;
    }

    @Override
    public void save(Status status) {
        if (status.isNew()){
            entityManager.persist(status);
            logger.info("Status successfully saved. Status info: " + status);
        }else {
            entityManager.merge(status);
            logger.info("Status successfully updated. Status info: " + status);
        }
    }

    @Override
    public void remove(Status status) {
        entityManager.remove(entityManager.merge(status));
        logger.info("Status removed. Status info: " + status);
    }
}
