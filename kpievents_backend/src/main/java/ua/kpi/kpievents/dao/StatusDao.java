package ua.kpi.kpievents.dao;

import ua.kpi.kpievents.model.Status;

import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */
public interface StatusDao extends GenericDAO<Status, UUID>{

    Status findByStatusName(String name);
}
