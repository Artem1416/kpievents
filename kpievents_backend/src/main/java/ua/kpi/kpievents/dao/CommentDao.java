package ua.kpi.kpievents.dao;

import ua.kpi.kpievents.model.Comment;
import ua.kpi.kpievents.model.User;

import java.util.Collection;
import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */
public interface CommentDao extends GenericDAO<Comment, UUID>{

    Collection<Comment> findByAuthor(User author);

}
