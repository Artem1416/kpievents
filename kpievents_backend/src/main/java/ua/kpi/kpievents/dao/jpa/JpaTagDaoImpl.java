package ua.kpi.kpievents.dao.jpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import ua.kpi.kpievents.dao.TagDao;
import ua.kpi.kpievents.model.Tag;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */

@Repository
public class JpaTagDaoImpl implements TagDao{

    @PersistenceContext
    private EntityManager entityManager;

    private static final Logger logger = LoggerFactory.getLogger(JpaTagDaoImpl.class);

    @Override
    public Tag findByName(String name) {
        Query query = entityManager.createQuery("SELECT tag FROM Tag tag WHERE tag.name = :name");
        query.setParameter("name", name);

        Tag tag = (Tag) query.getSingleResult();
        logger.info("Tag successfully loaded. Info: " + tag);
        return tag;
    }

    @Override
    public Tag getById(UUID id) {
        Query query = entityManager.createQuery("SELECT tag FROM Tag tag WHERE tag.id = :id");
        query.setParameter("id", id);
        Tag tag = (Tag) query.getSingleResult();
        logger.info("Tag successfully loaded. Info: " + tag);
        return tag;
    }

    @Override
    public Collection<Tag> getAll() {
        Query query = entityManager.createQuery("SELECT tag FROM Tag tag");
        Collection<Tag> tags = query.getResultList();
        for (Tag tag: tags) {
            logger.info("Result list: " + tag);
        }
        return tags;
    }

    @Override
    public void save(Tag tag) {
        if (tag.isNew()){
            entityManager.persist(tag);
            logger.info("Tag successfully saved. Tag info: " + tag);
        }else {
            entityManager.merge(tag);
            logger.info("Tag successfully updated. Tag info: " + tag);
        }
    }

    @Override
    public void remove(Tag tag) {
        entityManager.remove(entityManager.merge(tag));
    }
}
