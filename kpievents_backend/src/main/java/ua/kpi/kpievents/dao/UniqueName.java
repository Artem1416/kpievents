package ua.kpi.kpievents.dao;


import ua.kpi.kpievents.model.BaseEntity;

/**
 * UniqueName DAO interface. Extends the interface GenericDAO and
 * add new method for search by unique name.
 *
 * @author Eugene Suleimanov
 */

public interface UniqueName<T extends BaseEntity, ID> extends GenericDAO<T, ID> {
    T findByName(String name);
}
