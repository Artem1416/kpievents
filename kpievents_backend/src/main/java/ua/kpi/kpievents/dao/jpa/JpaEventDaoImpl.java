package ua.kpi.kpievents.dao.jpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import ua.kpi.kpievents.dao.EventDao;
import ua.kpi.kpievents.model.Category;
import ua.kpi.kpievents.model.Event;
import ua.kpi.kpievents.model.User;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */
@Repository
public class JpaEventDaoImpl implements EventDao {

    @PersistenceContext
    private EntityManager entityManager;

    private static final Logger logger = LoggerFactory.getLogger(JpaEventDaoImpl.class);

    @Override
    public Event getById(UUID id) {
        Query query = entityManager.createQuery("SELECT event FROM Event event WHERE event.id=:id");
        query.setParameter("id", id);

        Event event = (Event) query.getSingleResult();
        logger.info("Event successfully loaded. Event details: ");
        return event;
    }

    @Override
    public Collection<Event> getAll() {
        Query query = entityManager.createQuery("SELECT event FROM Event event");
        List<Event> events = query.getResultList();
        logger.info("All events successfully loaded.");
        for (Event event : events) {
            logger.info("event list: " + event);
        }
        return events;
    }

    @Override
    public void save(Event event) {
        if (event.isNew()) {
            entityManager.persist(event);
            logger.info("Event successfully saved. Event details: " + event);
        } else {
            entityManager.merge(event);
            logger.info("Event successfully updated. Event details: " + event);
        }
    }

    @Override
    public void remove(Event event) {
        entityManager.remove(entityManager.merge(event));
        logger.info("Event successfully removed. Event details: " + event);
    }

    @Override
    public Event findEventByName(String name) {
        Query query = entityManager.createQuery("SELECT event FROM Event event WHERE event.name=:name");
        query.setParameter("name", name);
        return (Event) query.getSingleResult();
    }

    @Override
    public Collection<Event> findEventsByCreatedUser(User user) {
        Query query = entityManager.createQuery("SELECT event FROM Event event WHERE event.createdUser=:user");
        query.setParameter("user", user);
        return query.getResultList();
    }

    @Override
    public Collection<Event> findEventsByDate(Date startDate, Date endDate) {
        Query query = entityManager.createQuery(
                "SELECT event FROM Event event WHERE event.startDate BETWEEN :startDate AND :endDate");
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        return query.getResultList();
    }

    @Override
    public Collection<Event> findEventsByCategory(Category category) {
        Query query = entityManager.createQuery("SELECT event FROM Event event WHERE event.category = :category");
        query.setParameter("category", category);

        Collection<Event> events = query.getResultList();
        for (Event e:events) {
            logger.info("Result list: " + e);
        }
        return events;
    }
}
