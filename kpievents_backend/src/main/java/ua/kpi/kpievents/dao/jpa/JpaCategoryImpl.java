package ua.kpi.kpievents.dao.jpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import ua.kpi.kpievents.dao.CategoryDao;
import ua.kpi.kpievents.model.Category;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */
@Repository
public class JpaCategoryImpl implements CategoryDao {

    @PersistenceContext
    private EntityManager entityManager;

    private static final Logger logger = LoggerFactory.getLogger(JpaCategoryImpl.class);


    @Override
    public Category findCategoryByName(String name) {
        Query query = entityManager.createQuery("SELECT category FROM Category category WHERE category.name = :name");
        query.setParameter("name", name);

        Category category = (Category) query.getSingleResult();
        logger.info("Category successfully loaded. Result: " + category);
        return category;
    }

    @Override
    public Category getById(UUID id) {
        Query query = entityManager.createQuery("SELECT category FROM Category category WHERE category.id = :id");
        query.setParameter("id", id);

        Category category = (Category) query.getSingleResult();
        logger.info("Category successfully loaded. Result: " + category);
        return category;
    }

    @Override
    public Collection<Category> getAll() {
        Query query = entityManager.createQuery("SELECT category FROM Category category");
        Collection<Category> categories = query.getResultList();

        for (Category c : categories) {
            logger.info("result list: " + c);
        }
        return categories;
    }

    @Override
    public void save(Category category) {
        if (category.isNew()) {
            entityManager.persist(category);
            logger.info("Category successfully saved");
        } else {
            entityManager.merge(category);
            logger.info("Category successfully update");
        }
    }

    @Override
    public void remove(Category category) {
        entityManager.remove(entityManager.merge(category));
        logger.info("category deleted");
    }
}
