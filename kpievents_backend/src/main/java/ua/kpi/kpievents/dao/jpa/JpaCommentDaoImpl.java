package ua.kpi.kpievents.dao.jpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.spel.ast.QualifiedIdentifier;
import org.springframework.stereotype.Repository;
import ua.kpi.kpievents.dao.CommentDao;
import ua.kpi.kpievents.model.Comment;
import ua.kpi.kpievents.model.User;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.UUID;

/**
 * @author Alexey Yefimenko
 */

@Repository
public class JpaCommentDaoImpl implements CommentDao{

    @PersistenceContext
    private EntityManager entityManager;

    private static final Logger logger = LoggerFactory.getLogger(JpaCommentDaoImpl.class);

    @Override
    public Comment getById(UUID id) {
        Query query = entityManager.createQuery("SELECT comment FROM Comment comment WHERE comment.id = :id");
        query.setParameter("id", id);

        Comment comment = (Comment) query.getSingleResult();
        logger.info("Comment successfully loaded. Comment info: " + comment);

        return comment;
    }

    @Override
    public Collection<Comment> getAll() {
        Query query = entityManager.createQuery("SELECT comment FROM Comment comment");
        Collection<Comment> comments = query.getResultList();
        for (Comment c:comments) {
            logger.info("Result list: " + c);
        }
        return comments;
    }

    @Override
    public void save(Comment comment) {
        if (comment.isNew()){
            entityManager.persist(comment);
            logger.info("Comment successfully saved. Comment info: " + comment);
        }else {
            entityManager.merge(comment);
            logger.info("Comment successfully updated. Comment info: " + comment);
        }
    }

    @Override
    public void remove(Comment status) {
        entityManager.remove(entityManager.merge(status));
    }

    @Override
    public Collection<Comment> findByAuthor(User author) {
        Query query = entityManager.createQuery("SELECT comment FROM Comment comment WHERE comment.author = :author");
        query.setParameter("author", author);

        Collection<Comment> comments = query.getResultList();
        for (Comment c:comments) {
            logger.info("Result list: " + author);
        }

        return comments;
    }
}
