package ua.kpi.kpievents.exception;

/**
 * Created by Yuriy Phediv on 23.03.2017.
 */
public class BadRequestException extends Exception {

    private String errorMsg;

    public BadRequestException(){
        super();
    }

    public BadRequestException(String msg){
        super(msg);
        this.errorMsg = msg;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
