CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- users
CREATE TABLE IF NOT EXISTS users (
  id                UUID         NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  email             VARCHAR(255) NOT NULL,
  username          VARCHAR(50)  NOT NULL,
  first_name        VARCHAR(50)  NOT NULL,
  last_name         VARCHAR(50)  NOT NULL,
  password          VARCHAR(255) NOT NULL,
  registration_date TIMESTAMP    NOT NULL,
  birth_date        TIMESTAMP
);

-- roles
CREATE TABLE IF NOT EXISTS roles (
  id   UUID        NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  name VARCHAR(50) NOT NULL
);

-- user_roles
CREATE TABLE IF NOT EXISTS user_roles (
  user_id UUID NOT NULL,
  role_id UUID NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users (id),
  FOREIGN KEY (role_id) REFERENCES roles (id),

  UNIQUE (user_id, role_id)
);

CREATE TABLE IF NOT EXISTS statuses (
  id   UUID        NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  name VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS categories (
  id   UUID        NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  name VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS tags (
  id   UUID        NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  name VARCHAR(50) NOT NULL
);


CREATE TABLE IF NOT EXISTS events (
  id            UUID          NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  name          VARCHAR(100)  NOT NULL,
  description   VARCHAR(1000) NOT NULL,
  start_date    TIMESTAMP     NOT NULL,
  end_date      TIMESTAMP     NOT NULL,
  address       VARCHAR(1000),
  admin_comment VARCHAR(1000),
  map_link      VARCHAR(255),
  website       VARCHAR(255),
  vk_link       VARCHAR(255),
  fb_link       VARCHAR(255),
  creator_id    UUID          NOT NULL,
  status_id     UUID          NOT NULL,
  category_id   UUID          NOT NULL,

  FOREIGN KEY (creator_id) REFERENCES users (id),
  FOREIGN KEY (status_id) REFERENCES statuses (id),
  FOREIGN KEY (category_id) REFERENCES categories (id)

);

CREATE TABLE IF NOT EXISTS event_tags (
  event_id UUID NOT NULL,
  tag_id   UUID NOT NULL,

  FOREIGN KEY (event_id) REFERENCES events (id),
  FOREIGN KEY (tag_id) REFERENCES tags (id),

  UNIQUE (event_id, tag_id)
);

CREATE TABLE IF NOT EXISTS comments (
  id        UUID          NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
  text      VARCHAR(1000) NOT NULL,
  author_id UUID          NOT NULL,
  event_id  UUID          NOT NULL,

  FOREIGN KEY (author_id) REFERENCES users (id),
  FOREIGN KEY (event_id) REFERENCES events (id)
);